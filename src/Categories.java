
public class Categories {
	private int id;
	private String name;
	private boolean expensive;
	
	public Categories(int id, String name, boolean expensive) {
		super();
		this.id = id;
		this.name = name;
		this.expensive = expensive;
	}

	public Categories() {
		super();
	}
	
	//contoh Getter dan Setter tipe data boolean 
	//getter 
	public boolean isExpensive() {
		return this.expensive;
	}
	
	//setter
	public boolean setExpensive() {
		return this.expensive;
	}
	
	//tipe data object (string)
	//getter
	public String getNama() {
		return this.name;
	}
	
	//setter 
	public void setNama(String nama) {
		this.name = name;
	}
	
	//tipe data primitf (int)
	//getter 
	public int getId() {
		return this.id;
	}
	
	//setter 
	public void setId(int id) {
		this.id = id;
	}
	
}
