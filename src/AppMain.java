/*Created By : Ilham Nur Rizki
* Created At : 16 juni 2023 
*/

import parents.Person;
import childs.Doctor;
import childs.Programmer;
import childs.Teacher;

public class AppMain {

	public static void main(String[] args) {
		//menset object 
		Person person1 = new Person();
		person1.setName("Ilham");
		person1.setAddress("Bandung");
		
		//cara memanggil / menggunakan field name
		System.out.println(person1.getName());
		System.out.println(person1.getAddress());
	}
	
	static void sayHello(Person person) {
	}

}
