package childs;
import parents.Person;

public class Programmer extends Person {
	public String technology;
	
	public Programmer() {
		
	}
	
	public Programmer(String name , String address, String technology) {
		super(name, address);
		this.technology = technology;
	}
	
	public void hacking() {
		System.out.println("I can hacking a website");
	}
	
	public void coding() {
		System.out.println("I can Develop a application using techology" + technology + ".");
		
	}
	
	@Override
	public void greeting() {
		super.greeting();//memanggil method greeting dari personclass
		System.out.println("My job is a " + technology +" Programmer.");
	}
	
	//getter dan setter 
	public String getTechnology() {
		return this.technology;
	}
	
	public void setTechnology(String technology) {
		this.technology = technology;
	}
}
